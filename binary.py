from binary_tests import ex


@ex(0)
def inc(n):
    return n + 1


@ex(1)
def to_bin(decimal):
    pass

# Write the rest of your solutions here.

# EXTRA Assignment:
# -----------------
# Try to write code that prints out the number to binary conversion between 0 and 32 using the while statement
# and the print() function.

# You can run the test suite as a script from the command-line with:
# $ python3.3 binary.py
if __name__ == "__main__":
    ex.test()