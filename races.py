# A Day at the Races
#
# Note
# ====
# Unless otherwise specified all units are in SI:
#    - distance in meters (m)
#    - time in seconds (s)
#    - speed in meters per second (m/s)
#
# Exercise
# ========
# 1. Write a function that converts kilometers per hour to meters per second.
#    Round to one decimal.
# 2. Write a function that converts meters per second to kilometers per hour.
#    Round to one decimal.
# 3. Write a function that calculates the distance travelled over a given time
#    at a given speed.
# 4. Write a function that calculates the average speed given a distance over
#    time. Round to one decimal.
# 5. Write a function that calculates the time it takes to travel a distance at
#    a given speed. Round to one decimal.
#
# 6. Write a function that simulates a car travelling a given distance.
#    - the car starts standing still (speed = 0)
#    - the car has a maximum speed
#    - the car has an acceleration rate: each second the speed of the increases
#      by some amount up to the maximum speed.
#      the default acceleration rate is 20
#    - the function shall return the number of seconds it takes the car to
#      clear the given distance.
#
#    Example:
#    --------
#    A Ferrari F60  has a max speed of 389km/h. It accelerates at the default
#    rate of 20. To get 800m it takes the car:
#      * 1s to go from speed 0 to 20 (distance = 20)
#      * 1s to go from 20 to 40 (distance = 20 + 40)
#      * 1s to go from 40 to 60 (distance = 20 + 40 + 60)
#      * ...
#      * 1s to go from 160 to 180 (distance = 20 + 40 + 60 + 80 + 100 + 120 +
#        140 + 160 + 180)
#
#    It takes the Ferrari F60 9s to go 800m from standing still.

from races_tests import ex


@ex(1)
def to_ms(kmph):
    return round(kmph * 1000 / 60 / 60, 1)


@ex(2)
def to_kmph(ms):
    pass

# Write the rest of the functions here.

# You can run the test suite as a script from the command-line with:
# $ python3.3 races.py
if __name__ == "__main__":
    ex.test()